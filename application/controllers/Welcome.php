<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model(['Ovni', 'Funcionario']);

		$grupos = [
			'HALLEY' => 'AMARELO',
			'ENCKE' => 'VERMELHO',
			'WOLF' => 'PRETO',
			'KUSHIDA' => 'AZUL'
		];

		$funcionarios = [
			[
				'nome' => 'Func 1',
				'data_admissao' => '01/01/2014'
			],
			[
				'nome' => 'Func 2',
				'data_admissao' => '11/07/2013'
			],
			[
				'nome' => 'Func 3',
				'data_admissao' => '15/06/2014'
			],
			[
				'nome' => 'Func 4',
				'data_admissao' => '21/07/2015'
			],
			[
				'nome' => 'Func 5',
				'data_admissao' => '01/02/2015'
			]
		];
		
		$data['gruposNaoEnviados'] = $this->Ovni->recuperarGruposNaoEnviados($grupos);
		$data['funcionarios'] = $this->Funcionario->calcularPeriodos($funcionarios);
		
		$this->load->view('welcome_message', $data);
	}
}
