<?php
class Funcionario extends CI_Model
{
	public function calcularPeriodos($funcionarios)
	{
		foreach ($funcionarios as &$funcionario) {
			$funcionario['periodo_aquisitivo'] = $this->periodoAquisitivo($funcionario['data_admissao']);
			$funcionario['periodo_concessivo'] = $this->periodoConcessivo($funcionario['data_admissao']);
		}
		return $funcionarios;
	}

	private function periodoAquisitivo($data_admissao)
	{
		$data_admissao = $this->formataData($data_admissao);

		$final_periodo_aquisitivo = clone $data_admissao;

		$final_periodo_aquisitivo->add(new DateInterval('P364D'));

		return 'De ' . $data_admissao->format('d/m/y') . ' até ' . $final_periodo_aquisitivo->format('d/m/y');
	}

	private function periodoConcessivo($data_admissao)
	{
		$data_admissao = $this->formataData($data_admissao);
		
		$inicio_periodo_concessivo = clone $data_admissao;
		$final_periodo_concessivo  = clone $data_admissao;

		$inicio_periodo_concessivo->add(new DateInterval('P365D'));
		$final_periodo_concessivo->add(new DateInterval('P729D'));
		
		return 'De ' . $inicio_periodo_concessivo->format('d/m/y') . ' até ' . $final_periodo_concessivo->format('d/m/y');
	}

	private function formataData($data)
	{
		$data = explode('/', $data);
		$data = $data[2] . '-' . $data[1] . '-' . $data[0];
		return new DateTime($data);
	}
}