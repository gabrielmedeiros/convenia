<?php
class Ovni extends CI_Model
{
	const RESTO = 45;

	public function recuperarGruposNaoEnviados($grupos)
	{
		$retorno = [];
		foreach ($grupos as $cometa => $grupo) {
			$vlr_cometa = $this->calcularProduto($cometa);
			$vlr_grupo  = $this->calcularProduto($grupo);

			if (($vlr_cometa % Ovni::RESTO) != ($vlr_grupo % Ovni::RESTO)) {
				$retorno[$cometa] = $grupo;
			}
		}

		return $retorno;
	}

	private function calcularProduto($texto)
	{
		$alfabeto = range('A', 'Z');

		$total = 1;
		for ($i=0; $i < strlen($texto); $i++) { 
			$total *= array_search($texto[$i], $alfabeto) + 1;
		}

		return $total;
	}
}