<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>
<body>
	<table>
		<thead>
			<th>Cometa</th>
			<th>Grupo</th>
		</thead>
		<tbody>
			<?php foreach ($gruposNaoEnviados as $cometa => $grupo) : ?>
			<tr>
				<td><?php echo $cometa ?></td>
				<td><?php echo $grupo ?></td>
			</tr>
			<?php endforeach ?>
		</tbody>
	</table>

	<table>
		<thead>
			<th>Funcionário</th>
			<th>Data Admissão</th>
			<th>Período Aquisitivo</th>
			<th>Período Concessivo</th>
		</thead>
		<tbody>
			<?php foreach ($funcionarios as $funcionario) : ?>
			<tr>
				<td><?php echo $funcionario['nome'] ?></td>
				<td><?php echo $funcionario['data_admissao'] ?></td>
				<td><?php echo $funcionario['periodo_aquisitivo'] ?></td>
				<td><?php echo $funcionario['periodo_concessivo'] ?></td>
			</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</body>
</html>